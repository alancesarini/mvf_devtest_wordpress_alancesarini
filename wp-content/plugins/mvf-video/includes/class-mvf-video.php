<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class MVF_Video {

	/**
	 * Instance of this class
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
    private static $_instance = null;
    
	/**
	 * The version number of this class
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
    public $_version;
    
	/**
	 * Prefix used to avoid naming conflicts
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
    public $prefix;

    /**
	 * The main plugin file.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $file;

	/**
	 * The main plugin directory.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $dir;

	/**
	 * The plugin assets directory.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $assets_dir;

	/**
	 * The plugin assets URL.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $assets_url;
	
	/**
	 * Video types accepted by the plugin
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
    public $video_types;	
    
	/**
	 * The constructor
	 * @access  public
	 * @since   1.0.0
	 * @param  string $file   	name of the main file
	 * @param  string $version	version number
	 * @return  void
	 */
	public function __construct ( $file = '', $version = '1.0.0' ) {
		// Initialise object properties
		$this->_version = $version;
		$this->prefix = 'mvf_video_';
        $this->file = $file;
		$this->dir = dirname( $this->file );
		$this->assets_dir = trailingslashit( $this->dir ) . 'assets';
        $this->assets_url = esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
		$this->file = $file;
		$this->dir = dirname( $this->file );
		$this->assets_dir = trailingslashit( $this->dir ) . 'assets';
		$this->assets_url = esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
		$this->video_types = array( 'YouTube', 'Vimeo', 'Dailymotion' );

		// Load assets
		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ), 10 );

		// Load textdomain
        add_action( 'init', array( $this, 'load_plugin_textdomain' ), 0 );
        
        // Register CPT
		add_action( 'init', array( $this, 'register_cpt' ) );
		
		// Add a metabox for the custom fields
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );

		// Save the meta fields
		add_action( 'save_post', array( $this, 'save_meta_fields' ) );

		// Render any errors in the backoffice
		add_action( 'admin_notices', array( $this, 'render_admin_errors' ) );

		// Register the shortcode to show a video
		add_shortcode( 'prefix_video', array( $this, 'create_shortcode' ) );

		// Hide the CPT if the user is an author
		add_action( 'admin_menu', array( $this, 'remove_from_menu' ) );


    } 

    /**
	 * Create the instance for this class
	 *
	 * @access public
	 * @since 1.0.0
	 * @static
	 * @param  string $file   	name of the main file
	 * @param  string $version	version number
	 * @return Main MVF_Video instance
	 */
	public static function instance ( $file = '', $version = '1.0.0' ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $file, $version );
		}
		return self::$_instance;
    }
    
	/**
	 * Enqueue JS and CSS files for the frontend.
	 * @access  public
	 * @since   1.0.0
	 * @return void
	 */
	public function load_assets () {
		wp_register_style( $this->prefix . 'main' , esc_url( $this->assets_url ) . 'css/main.css', array(), $this->_version );
        wp_enqueue_style( $this->prefix . 'main' );    
	}

	/**
	 * Load textdomain
	 * @access  public
	 * @since   1.0.0
	 * @return  void
	 */
	public function load_plugin_textdomain () {
	    $domain = 'mvf-video';
	    $locale = apply_filters( 'plugin_locale', get_locale(), $domain );
	    load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo' );
	    load_plugin_textdomain( $domain, false, dirname( plugin_basename( $this->file ) ) . '/lang/' );
    }   
    
	/**
	 * Register the video CPT
	 * @access  public
	 * @since   1.0.0
	 * @return void
	 */ 
    public function register_cpt() {
        $labels = array(
            'name'               => __( 'MVF Videos', 'mvf-video' ),
            'singular_name'      => __( 'MVF Video', 'mvf-video' ),
            'add_new'            => __( 'Add new', 'mvf-video' ),
            'add_new_item'       => __( 'Add new video', 'mvf-video' ),
            'edit_item'          => __( 'Edit video', 'mvf-video' ),
            'new_item'           => __( 'New video', 'mvf-video' ),
            'all_items'          => __( 'All videos', 'mvf-video' ),
            'view_item'          => __( 'View video', 'mvf-video' ),
            'search_items'       => __( 'Search videos', 'mvf-video' ),
            'not_found'          => __( 'No videos found', 'mvf-video' ),
            'not_found_in_trash' => __( 'No videos found in trash', 'mvf-video' ), 
            'parent_item_colon'  => '',
            'menu_name'          => 'MVF Videos'
        );
        $args = array(
            'labels'        => $labels,
            'description'   => 'CPT for MVF videos',
            'public'        => true,
            'menu_position' => 21,
            'hierarchical'  => false,
            'supports'      => array( 'title', 'editor' ),
            'has_archive'   => false,
            'exclude_from_search' => true,
            'publicaly_queryable' => false,
            'query_var' => false	
        );
        register_post_type( 'mvf_video', $args );		
	}  

	/**
	 * Add the metabox for the custom fields
	 * @access  public
	 * @since   1.0.0
	 * @return void
	 */ 
	public function add_metabox() {
		add_meta_box(
			'mvf_video_metabox',
			__( 'Custom fields', 'mvf-video' ),
			array( $this, 'render_custom_fields' ),
			'mvf_video'
		);
	}	
	
	/**
	 * Render the custom fields
	 * @access  public
	 * @since   1.0.0
	 * @param  string $post   the current post 
	 * @return void
	 */ 
	public function render_custom_fields( $post ) {
		// Set the nonce
		wp_nonce_field( basename( $this->file ), 'mvf_nonce' ); 

		// Set the custom field names
		$meta_subtitle = $this->prefix . 'subtitle';
		$meta_type = $this->prefix . 'type';
		$meta_video_embed = $this->prefix . 'embed';
		
		// Get the custom field values
		$subtitle = get_post_meta( $post->ID, $meta_subtitle, true );
		$type = get_post_meta( $post->ID, $meta_type, true );
		$video_embed = get_post_meta( $post->ID, $meta_video_embed, true );	
		
		// Render the custom fields
	?>				
		<p>
			<label for="<?php echo $meta_subtitle; ?>"><?php _e( 'Subtitle', 'mvf-video' ); ?>:</label><br/>
			<input type="text" name="<?php echo $meta_subtitle; ?>" id="<?php echo $meta_subtitle; ?>" value="<?php echo $subtitle; ?>" class="large-text" />
		</p>
		<p>
			<label for="<?php echo $meta_type; ?>"><?php _e( 'Type', 'mvf-video' ); ?>:</label><br/>
			<select name="<?php echo $meta_type; ?>" id="<?php echo $meta_type; ?>">
				<option value="">Select video type</option>
				<?php foreach( $this->video_types as $video_type ) { ?>
					<option value="<?php echo $video_type; ?>" <?php selected( $type, $video_type ); ?>><?php echo $video_type; ?></option>
				<?php } ?>
			</select>		
		</p>
		<p>
			<label for="<?php echo $meta_video_embed; ?>"><?php _e( 'Embed code', 'mvf-video' ); ?>:</label><br/>
			<textarea name="<?php echo $meta_video_embed; ?>" id="<?php echo $meta_video_embed; ?>" class="large-text"><?php echo $video_embed; ?></textarea>
		</p>
	<?php			
	}

	/**
	 * Save the custom fields
	 * @access  public
	 * @since   1.0.0
	 * @param  string $post_id	the current post id	 
	 * @return void
	 */ 
	public function save_meta_fields( $post_id ) {	
		// If this post is not of type "video", abort
		$current_post = get_post( $post_id );
		$post_type = get_post_type_object( $current_post->post_type );
		if( $post_type->name != 'mvf_video' )
			return $post_id;
		
		// check if there have been any errors
		$error_code = '';

		// Make sure the nonce has been set
		if ( !isset( $_POST[ 'mvf_nonce' ] ) || !wp_verify_nonce( $_POST[ 'mvf_nonce' ], basename( $this->file ) ) ) {
			$error_code = 'nonce-error';
		}
		
		// Make sure the user has permission to edit this post type
		if( '' == $error_code ) {
			if ( !current_user_can( $post_type->cap->edit_post, $post_id ) ) {
				$error_code = 'user-error';
			}
		}

		// Check if there are custom fields missing
		if( '' == $error_code ) {		
			$meta_keys = array( $this->prefix . 'subtitle', $this->prefix . 'type', $this->prefix . 'embed' );
			foreach( $meta_keys as $meta_key ) {	
				if( !array_key_exists( $meta_key, $_POST ) || '' == $_POST[$meta_key] ) {
					$error_code = 'field-error';
				} else {
					$new_meta_value = $_POST[ $meta_key ];
					update_post_meta( $post_id, $meta_key, $new_meta_value );					
				}
			}	
		}	 
		
		if( $error_code != '' ) {		
			// Add a param to the query to show the error messages
			add_filter( 'redirect_post_location', function( $location ) use ( $error_code ) {
				return add_query_arg( 'mvf-error', $error_code, $location );
			});			
		}
	}

	/**
	 * Render any errors
	 * @access  public
	 * @since   1.0.0
	 * @return void
	 */ 
	public function render_admin_errors() {
		if ( array_key_exists( 'mvf-error', $_GET) ) { ?>
			<div class="error">
				<p>
					<?php
						switch($_GET['mvf-error']) {
							case 'nonce-error':
								echo __( 'There has been a problem with the nonce', 'mvf-video' );
								break;
							case 'user-error':
								echo __( "You don't have permission to edit the video", 'mvf-video' );
								break;
							case 'field-error':
								echo __( 'Please fill all the custom fields', 'mvf-video' );
								break;
						}
					?>
				</p>
			</div><?php
		}
	}

	/**
	 * Create the shortcode for rendering the video
	 * @access  public
	 * @since   1.0.0
	 * @param $atts	the shortcode params
	 * @return void
	 */ 
	public function create_shortcode( $atts ) {
		// Initialise params
		$params = shortcode_atts( array(
			'id' => '',
			'border_color' => '#3498db'
		), $atts );

		// If there is no video id, abort
		if( '' == $params['id'] )
			return;

		// Get the video
		$video = get_post( $params['id']);

		// If the video doesn't exist, abort
		if( null == $video )
			return;

		// Get the video data
		$title = get_the_title( $video );
		$description = get_post_field( 'post_content', $video->ID );
		$subtitle = get_post_meta( $video->ID, $this->prefix . 'subtitle', true );
		$embed = get_post_meta( $video->ID, $this->prefix . 'embed', true );
		$border_color = $params['border_color'];

		// Layout
		$html = "
			<div class='mvf-video-container' style='border: 8px solid $border_color'>
				<div class='video'>
					<div class='video-container'>
						$embed
					</div>
				</div>
				<div class='content'>
					<h2>$title</h2>
					<h3>$subtitle</h3>
					$description
				</div>
			</div>";

		return $html;
	}

	/**
	 * Removes the CPT from the backoffice menu if the current user has the role "author"
	 * @access  public
	 * @since   1.0.0
	 * @param $atts	the shortcode params
	 * @return void
	 */ 
	public function remove_from_menu() {
		$user = wp_get_current_user();
		if( in_array( 'author', $user->roles ))	{	
			remove_menu_page( 'edit.php?post_type=mvf_video' );
		}
	}	
}