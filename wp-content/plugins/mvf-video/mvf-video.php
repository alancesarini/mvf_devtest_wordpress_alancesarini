<?php
/*
 * Plugin Name: MVF Video
 * Version: 1.0
 * Description: Register a CPT that represents a video, and a shortcode to show that video
 * Author: Alan Cesarini
 * Author URI: http://www.alancesarini.com/
 * Requires at least: 4.7
 * Tested up to: 4.7
 *
 * Text Domain: mvf_video
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Alan Cesarini
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load the video class file
require_once( 'includes/class-mvf-video.php' );

/**
 * Returns the main instance of WordPress_Plugin_Template to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object WordPress_Plugin_Template
 */
function MVF_Plugin_Video () {
	$instance = MVF_Video::instance( __FILE__, '1.0.0' );

    /*
	if ( is_null( $instance->settings ) ) {
		$instance->settings = WordPress_Plugin_Template_Settings::instance( $instance );
    }
    */

	return $instance;
}

MVF_Plugin_Video();